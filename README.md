# Group Project 2

###Maven Installation
<ul>
<li>Download and unzip maven (http://maven.apache.org/download.cgi)</li>
<li>set the M2_HOME variable to the root install directory for maven</li>
<li>add $M2_HOME/bin to the path (*nix) or %M2_HOME%\bin (Windows)</li>
</ul>

##Running Stuff
Make sure you are in the root of the project code:

###To generate jacoco report
mvn clean test jacoco:report
reports will be under {PROJECT_HOME}/target/site/index.html

###To run mutation tests
mvn org.pitest:pitest-maven:mutationCoverage
reports will be under {PROJECT_HOME}/target/pit-reports/{SOMETIMESTAMP}/index.html
