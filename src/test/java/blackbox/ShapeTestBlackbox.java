package blackbox;


import analysis.ShapeClassifier;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.mappers.CsvWithHeaderMapper;
import org.apache.commons.lang3.StringUtils;
import org.junit.AfterClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class ShapeTestBlackbox {

    private static StringBuffer sbTestResults = new StringBuffer();

    @AfterClass
    public static void exportCsvResult() throws IOException {
        BufferedWriter resultWriter = Files.newBufferedWriter(Paths.get("blackBoxOutput.csv"),Charset.defaultCharset());
        resultWriter.write("shape, size, even, params  TestResult");
        resultWriter.newLine();
        resultWriter.write(sbTestResults.toString());
        resultWriter.close();
    }

    @Rule
    public static TestWatcher addCsvResult = new TestWatcher() {
        @Override
        protected void succeeded(Description description) {
            sbTestResults.append(", Passed").append(System.getProperty("line.separator"));
        }
        @Override
        protected void failed(Throwable e, Description description) {
            sbTestResults.append(", Failed").append(System.getProperty("line.separator"));
        }
    };


    @Ignore
	@Test
	@FileParameters(value = "src/test/resources/allTriples.csv",mapper = CsvWithHeaderMapper.class)
	public void test(String shape,String size,String even,String param1,String param2, String param3,String param4,String expectedResult){
        //        shape,size,even,length1,length2,length3,length4,expected



        List<String> l=new ArrayList<String>();
        if(!"*".equals(param1))
        l.add(param1);
        if(!"*".equals(param2))
        l.add(param2);
        if(!"*".equals(param3))
        l.add(param3);
        if(!"*".equals(param4))
        l.add(param4);


        List<String> completeParams=new ArrayList<>();

        completeParams.add(shape);
        completeParams.add(size);
        completeParams.add(even);
        completeParams.addAll(l);

        String separatedString=StringUtils.join(completeParams,",");
		sbTestResults.append(separatedString);


		ShapeClassifier classifier = new ShapeClassifier();


		String actualResult = classifier.evaluateGuess(separatedString);

        assertEquals(expectedResult,actualResult);

	}


    @Ignore
    @Test
    @FileParameters(value = "src/test/resources/blackboxTest2.csv",mapper = CsvWithHeaderMapper.class)
    public void testFull(String testNumber,String shape,String size,String even,String param1,String param2, String param3,String param4,String expectedResult){
        // Test Case#,shape,size,even,length1,length2,length3,length4,expected

        param1=param1.replace("*","0");
        param2=param2.replace("*","0");
        param3=param3.replace("*","0");
        param4=param4.replace("*","0");

        String separatedString=StringUtils.join(new String[]{shape,size,even,param1,param2,param3,param4},",");
        sbTestResults.append(separatedString);


        ShapeClassifier classifier = new ShapeClassifier();

        String actualResult = classifier.evaluateGuess(separatedString);

        assertEquals(expectedResult,actualResult);

    }

    @Test
    @FileParameters(value = "src/test/resources/allSingles.csv",mapper = CsvWithHeaderMapper.class)
    public void singlesTest(String shape,String size,String even,String params,String expectedResult){

        String [] inputs={shape,size,even,params};
        sbTestResults.append(StringUtils.join(inputs, ","));

        inputs[3] = inputs[3].replace("_", ",");

        ShapeClassifier classifier = new ShapeClassifier();

        String actualResult = classifier.evaluateGuess(StringUtils.join(inputs,","));

        assertEquals(expectedResult,actualResult);

    }





}
