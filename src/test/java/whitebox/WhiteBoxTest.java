package whitebox;


import analysis.ShapeClassifier;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.mappers.CsvWithHeaderMapper;
import org.apache.commons.lang3.StringUtils;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class WhiteBoxTest {

    private static StringBuffer sbTestResults = new StringBuffer();

    @AfterClass
    public static void exportCsvResult() throws IOException {
        BufferedWriter resultWriter = Files.newBufferedWriter(Paths.get("testOutput.csv"),Charset.defaultCharset());
        resultWriter.write("shape, size, even, TestResult");
        resultWriter.newLine();
        resultWriter.write(sbTestResults.toString());
        resultWriter.close();
    }

    @Rule
    public static TestWatcher addCsvResult = new TestWatcher() {
        @Override
        protected void succeeded(Description description) {
            sbTestResults.append(", Passed").append(System.getProperty("line.separator"));
        }
        @Override
        protected void failed(Throwable e, Description description) {
            sbTestResults.append(", Failed").append(System.getProperty("line.separator"));
        }
    };

	@Test
	@FileParameters(value = "src/test/resources/whiteboxTests.csv",mapper = CsvWithHeaderMapper.class)
	public void test(String shape,String size,String even,String params,String expectedResult,String path){

        String [] inputs={shape,size,even,params};
		sbTestResults.append(StringUtils.join(inputs, ","));

		 inputs[3] = inputs[3].replace("_", ",");
				
		ShapeClassifier classifier = new ShapeClassifier();

		String actualResult = classifier.evaluateGuess(StringUtils.join(inputs,","));

        assertEquals(expectedResult,actualResult);

	}


    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();


    /*@Test
    public final void testThreeCorrectGuesses() {

        ShapeClassifier classifier = new ShapeClassifier();

        //3 good guesses
        String actualResult = classifier.evaluateGuess("Line,Large,Yes,202");

        assertEquals("Yes",actualResult);

        actualResult = classifier.evaluateGuess("Circle,Large,Yes,52,52");

        assertEquals("Yes",actualResult);

        actualResult = classifier.evaluateGuess("Line,Small,No,3");

        assertEquals("Yes",actualResult);


    }

    @Test
    public final void testThreeFailedGuesses() {

        ShapeClassifier classifier = new ShapeClassifier();

        String actualResult = classifier.evaluateGuess("Line,Small,Yes,3");
        assertEquals("No",actualResult);
        //2 bad

        actualResult = classifier.evaluateGuess("Line,Large,No,12");
        assertEquals("No",actualResult);

        //3 bad, should exit

        exit.expectSystemExitWithStatus(1);
        classifier.evaluateGuess("Rectangle,Large,Yes,52,53,52,52");

    }*/
	/*
	 * path 21 - if parameters.length is default, then we have no lengths input, so we can't possibly have a perimeter greater than 200
	 * path 22 - same problem as path 21
	 * path 23 - impossible - if calcperim < 10 && sizeGuess.equals("Small") == false then issizeguesscorrect can NOT be true
	 */

}
