package edu.innopolis.gp;

import analysis.ShapeClassifier;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by d7vladislav on 3/26/17.
 */
public class ManualTestHelper {
	
    @Test
    public void manualTest(){
        ShapeClassifier s=new ShapeClassifier();
        String result=s.evaluateGuess("Scalene,Small,No,2,3,4");
        assertEquals("Yes",result);
    }
}